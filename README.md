# GitLab badges

<img src="language/en/en-available-on.png"
    alt="Available on GitLab"
    height="80">

Badges to show that a project is available on GitLab.

## Usage

HTML code:

```html
<a href="https://gitlab.com/<your-username>/<your-repo>">
    <img src="https://gitlab.com/narektor/gitlab-badges/-/raw/main/language/en/en-available-on.png"
    alt="Available on GitLab"
    height="80">
</a>
```

Markdown code:

```markdown
[<img src="https://gitlab.com/narektor/gitlab-badges/-/raw/main/language/en/en-available-on.png"
    alt="Available on GitLab"
    height="80">](https://gitlab.com/<your-username>/<your-repo>)
```

## Translating

The source files can be found in the [source](source) folder.

## Legal

> GITLAB is a trademark of GitLab Inc. in the United States and other countries and regions.

The images here use the GitLab name and wordmark to imply that a project is available, in binary or source code form, on a GitLab instance.

## Credits

The badge is based on [the F-Droid badges](https://gitlab.com/fdroid/artwork/-/tree/master/badge).
